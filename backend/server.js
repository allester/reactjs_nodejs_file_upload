const express = require('express')
const fileupload = require('express-fileupload')
const cors = require('cors')
const bodyParser = require('body-parser')
const fs = require('fs')

const app = express()

app.use(cors())
app.use(fileupload())
app.use(express.static("files"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))


app.post('/upload', (req, res) => {
    const newpath = __dirname + "files/"
    const file = req.files.file
    const filename = file.name

    file.mv(`${newpath}${filename}`, (err) => {
        if (err) {
            res.status(500).send({
                message: "File upload failed",
                code: 200
            })
        }
        
        res.status(200).send({
            message: "File uploaded",
            code: 200
        })
    })
})

app.listen(8000, () => {
    console.log(`Server running on port 8000`)
})