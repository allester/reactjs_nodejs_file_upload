import logo from './logo.svg';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import FileUpload from './FileUpload';

function App() {
  return (
    <div>
      <FileUpload />
    </div>
  );
}

export default App;
